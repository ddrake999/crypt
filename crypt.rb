# super-simple xor encryption/decryption
def crypt(str, key)
  str.split('').zip( (key*(str.length/key.length+1)).split('') ).map { |c1, c2| (c1.ord ^ c2.ord).chr }.join('')
end
