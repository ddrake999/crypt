require './crypt.rb'

describe "crypt method" do
  describe "with a long message and a short key" do
    let(:message) { "The dog has ears like an elephant!" }
    let(:key) { "Quaternion" }
    let(:encrypted) { crypt(message, key) }

    it "should encrypt the message" do
      expect(encrypted).to eql("\x05\x1D\x04T\x01\x1D\tI\a\x0F\"U\x04\x15\x17\x01N\x05\x06\x054U\x00\x1AE\x17\x02\f\x1F\x060\e\x15U")
    end

    it "should be able to decrypt the message" do
      expect(crypt(encrypted, key)).to eql(message)
    end

    it "should return a repeated key if the original and encrypted strings are passed" do
      expect(crypt(encrypted, message)).to eql("QuaternionQuaternionQuaternionQuat")
    end
  end

  describe "with a short message and a long key" do
    let(:message) { "Quaternion" }
    let(:key) { "The dog has ears like an elephant!" }
    let(:encrypted) { crypt(message, key) }
    
    it "should encrypt the message" do
      expect(encrypted).to eql("\x05\x1D\x04T\x01\x1D\tI\a\x0F")
    end

    it "should be able to decrypt the message" do
      expect(crypt(encrypted, key)).to eql(message)
    end

    it "should return a partial key if the original and encrypted strings are passed" do
      expect(crypt(encrypted, message)).to eql("The dog ha")
    end
  end

end